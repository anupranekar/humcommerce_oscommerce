HumCommerce Analytics module
Copyright (c) 2018 HumCommerce

==========================================================

For osCommerce version 2.3


DESCRIPTION:
------------

HumCommerce osCommerce plugin to Record, Analyze & Convert your visitors..


INSTALLATION:
--------------

For Installation please read the install.txt file.




DISCLAIMER
----------
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
(LICENSE) along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


AUTHOR
----------
HumCommerce

Support Mail : support@humcommerce.com







