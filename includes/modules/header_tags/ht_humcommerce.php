<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2013 osCommerce

  Released under the GNU General Public License
*/

  class ht_humcommerce {
    var $code = 'ht_humcommerce';
    var $group = 'header_tags';
    var $title;
    var $description;
    var $site_id;
    var $sort_order;
    var $enabled = false;

    function ht_humcommerce() {
      $this->title = 'HumCommerce Analytics';
      $this->description = '<img alt="HumCommerce" src="https://www.humcommerce.com/wp-content/uploads/2018/05/humcommerce-logo.png" style="height: 60px;">
                            <h2>Record visitors on your site.</h2>
                            <h3>Activate HumCommerce</h3>
                            <a href="https://www.humcommerce.com/#signup" target="_blank" style="text-decoration: underline; font-weight: bold;">Get your Site Id</a>&nbsp;</br>';

      $this->site_id = MODULE_HEADER_TAGS_HUMCOMMERCE_SITE_ID;
      $this->sort_order = MODULE_HEADER_TAGS_HUMCOMMERCE_SORT_ORDER;
      $this->enabled = (MODULE_HEADER_TAGS_HUMCOMMERCE_STATUS == 'True');
      //$this->sort_order = 100;
    }

    function execute() {
        global $oscTemplate;
        $oscTemplate->addBlock(
            '
        <!-- HumDash -->
        <script type="text/javascript">
          var _ha = _ha || [];
          /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
          _ha.push([\'trackPageView\']);
          _ha.push([\'enableLinkTracking\']);
          (function() {
            var u="//app.humdash.com/";
            _ha.push([\'setTrackerUrl\', u+\'humdash.php\']);
            _ha.push([\'setSiteId\', \''.$this->site_id.'\']);
            var d=document, g=d.createElement(\'script\'), s=d.getElementsByTagName(\'script\')[0];
            g.type=\'text/javascript\'; g.async=true; g.defer=true; g.src=u+\'humdash.js\'; s.parentNode.insertBefore(g,s);
          })();
        </script>
        <!-- End HumDash Code -->
        ',$this->group);
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return true;
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Site Id', 'MODULE_HEADER_TAGS_HUMCOMMERCE_SITE_ID', '*****', '<a href=\"https://www.humcommerce.com/docs/find-site-id-humcommmerce-tool/\" target=\"_blank\" style=\"text-decoration: underline; font-weight: bold;\">(What is Site Id?)</a>&nbsp;</br>', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_HEADER_TAGS_HUMCOMMERCE_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_HEADER_TAGS_HUMCOMMERCE_SITE_ID', 'MODULE_HEADER_TAGS_HUMCOMMERCE_SORT_ORDER');
    }
  }
?>
